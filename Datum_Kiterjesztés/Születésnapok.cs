﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datum_Kiterjesztés
{
    static class Születésnapok
    {
        static Dictionary<string, string> családiszületésnapok = new Dictionary<string, string>() {
                { "12.6." , "anya" },
                { "1.2.", "apa" },
                { "12.3.", "babuci" },
                { "4.16.","nagymama" },
                { "12.28.", "nagypapa" }
            };
        public static List<string> ünnepelt(this DateTime ma)
        {   List<string> kik = new List<string>();
            try
            {
                string[] szn;
                DateTime szülinap;               
                foreach (string datum in családiszületésnapok.Keys)
                {
                    szn = datum.Split(new char[] { '.' });
                    szülinap= new DateTime(ma.Year, Convert.ToInt16(szn[0]), Convert.ToInt16(szn[1]));
                    if (Math.Abs(((szülinap - ma)).Days) < 8)
                        kik.Add(családiszületésnapok[datum]);
                    else
                    {   //december-január közöttiek
                        szülinap = new DateTime(ma.Year + 1, Convert.ToInt16(szn[0]), Convert.ToInt16(szn[1]));
                        if (Math.Abs(((szülinap - ma)).Days) < 8)
                            kik.Add(családiszületésnapok[datum]);
                        else
                        {
                            szülinap = new DateTime(ma.Year - 1, Convert.ToInt16(szn[0]), Convert.ToInt16(szn[1]));
                            if (Math.Abs(((szülinap - ma)).Days) < 8)
                                kik.Add(családiszületésnapok[datum]);
                        }
                    } 
                }
            }
            catch { ; };
            return kik;
        }
    }
}
