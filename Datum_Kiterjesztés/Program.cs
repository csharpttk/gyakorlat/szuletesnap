﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datum_Kiterjesztés
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérem adja meg az aktuális dátumot év hó és nap formában pl. 2018.12.4. vagy 2017.12.31.");
            string[] d; 
            string szövegesdátum = Console.ReadLine();
            try
            {
                d = szövegesdátum.Split(new char[] { '.' }); //nincs dátum ellenőrzés!!
                DateTime dátum = new DateTime(Convert.ToInt32(d[0]), Convert.ToInt32(d[1]), Convert.ToInt32(d[2]));
                if (dátum.ünnepelt().Count() > 0)
                {
                    foreach (string ki in dátum.ünnepelt())
                    {
                        Console.Write("{0} ", ki);
                    }
                    Console.WriteLine();
                }
                else { Console.WriteLine("Nem volt senkinek 8 napon belül születésnapja"); }
            }
            catch {; }
        }
    }
}
